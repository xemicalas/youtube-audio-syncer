namespace YouTubeAudioSyncer.WebApi.Contracts
{
    /// <summary>
    /// Audio download data
    /// </summary>
    public class AudioDownloadData
    {
        /// <summary>
        /// Audio ID, which is equal to video ID
        /// </summary>
        public string AudioId { get; set; }
    }
}