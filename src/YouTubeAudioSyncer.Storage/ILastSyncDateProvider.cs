﻿using System;

namespace YouTubeAudioSyncer.Storage
{
    public interface ILastSyncDateProvider
    {
        DateTime Get();
    }
}