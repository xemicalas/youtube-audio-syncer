﻿using Microsoft.Win32;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Storage
{
    public class RegistryValueStorage : IValueStorage
    {
        private readonly string _registryStoragePath;

        public RegistryValueStorage(string registryStoragePath)
        {
            _registryStoragePath = registryStoragePath;
        }

        public object GetValue(string key)
        {
            RegistryKey registryKey = GetRegistryKey();
            object value = null;
            if (registryKey != null)
            {
                value = registryKey.GetValue(key);
                registryKey.Close();
            }
            return value;
        }

        public void PutValue(string key, object value)
        {
            RegistryKey registryKey = GetRegistryKey();
            if (registryKey != null)
            {
                registryKey.SetValue(key, value);
                registryKey.Close();
            }
        }

        private RegistryKey GetRegistryKey()
        {
            return Registry.LocalMachine.CreateSubKey(_registryStoragePath, true);
        }
    }
}