﻿using System;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Storage
{
    public class LastSyncDateProvider : ILastSyncDateProvider
    {
        private readonly IValueStorage _storage;

        public LastSyncDateProvider(IValueStorage storage)
        {
            _storage = storage;
        }

        public DateTime Get()
        {
            object value = _storage.GetValue("LastSyncDate");
            DateTime lastSyncDate = DateTime.UtcNow;
            if (value != null)
            {
                lastSyncDate = DateTime.Parse((string)value);
            }
            _storage.PutValue("LastSyncDate", DateTime.UtcNow);
            return lastSyncDate;
        }
    }
}