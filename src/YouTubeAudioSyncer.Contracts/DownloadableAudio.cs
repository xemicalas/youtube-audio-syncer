﻿namespace YouTubeAudioSyncer.Contracts
{
    public class DownloadableAudio
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string DestinationPath { get; set; }
    }
}