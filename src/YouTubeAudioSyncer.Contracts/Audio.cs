﻿using System;

namespace YouTubeAudioSyncer.Contracts
{
    public class Audio
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public DateTime PublishedAt { get; set; }
        public string Extension { get; set; }
        public MediaLocation OriginLocation { get; set; }
        public MediaLocation TempLocation { get; set; }
    }
}