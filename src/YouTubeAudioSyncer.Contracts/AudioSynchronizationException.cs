﻿using System;

namespace YouTubeAudioSyncer.Contracts
{
    public class AudioSynchronizationException : Exception
    {
        public AudioSynchronizationException(string message) : base(message)
        {
        }

        public AudioSynchronizationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}