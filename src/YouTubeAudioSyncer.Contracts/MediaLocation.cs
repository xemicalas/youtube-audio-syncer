﻿namespace YouTubeAudioSyncer.Contracts
{
    public class MediaLocation
    {
        public string DirectoryPath { get; set; }
        public string FullPath { get; set; }
    }
}