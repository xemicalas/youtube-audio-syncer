﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Client
{
    public class YouTubeClient : IYouTubeClient
    {
        private readonly YouTubeService _youtubeService;

        public YouTubeClient(string apiKey)
        {
            _youtubeService = new YouTubeService(new BaseClientService.Initializer
            {
                ApiKey = apiKey,
                ApplicationName = "YouTube Audio Syncer"
            });
        }

        public async Task<Audio> GetAudioClip(string audioId)
        {
            VideosResource.ListRequest request =  _youtubeService.Videos.List("snippet");
            request.Id = audioId;

            VideoListResponse response = await request.ExecuteAsync().ConfigureAwait(false);
            return response.Items.Select(MapAudio).FirstOrDefault();
        }

        public async Task<List<Audio>> GetAudioClips(string playListId, int limit)
        {
            PlaylistItemsResource.ListRequest request = _youtubeService.PlaylistItems.List("snippet");
            request.PlaylistId = playListId;
            request.MaxResults = limit;

            PlaylistItemListResponse response = await request.ExecuteAsync().ConfigureAwait(false);
            return response.Items.Select(MapAudio).ToList();
        }

        private Audio MapAudio(Video item)
        {
            return new Audio
            {
                Id = item.Id,
                Title = item.Snippet.Title,
                PublishedAt = (item.Snippet.PublishedAt ?? DateTime.MinValue).ToUniversalTime()
            };
        }

        private Audio MapAudio(PlaylistItem item)
        {
            return new Audio
            {
                Id = item.Snippet.ResourceId.VideoId,
                Title = item.Snippet.Title,
                PublishedAt = (item.Snippet.PublishedAt ?? DateTime.MinValue).ToUniversalTime()
            };
        }
    }
}