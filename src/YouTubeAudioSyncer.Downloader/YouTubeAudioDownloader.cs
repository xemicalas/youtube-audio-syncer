﻿using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Common.Logging;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Downloader
{
    public class YouTubeAudioDownloader : IAudioDownloader
    {
        private const string YouTubeUrl = "https://www.youtube.com/watch?v=";

        private readonly ILog _logger;

        public YouTubeAudioDownloader()
        {
            _logger = LogManager.GetLogger(GetType());
        }

        public Task Download(DownloadableAudio audio)
        {
            return Task.Run(() =>
            {
                _logger.Info($"Starting audio \"{audio.Title}\" download process...");
                Process process = CreateAudioDownloadProcess(audio);
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();
                if (process.ExitCode != 0)
                {
                    throw new AudioSynchronizationException($"Audio download process failed. Download process exit code: {process.ExitCode}.");
                }
            });
        }

        private Process CreateAudioDownloadProcess(DownloadableAudio audio)
        {
            string binaryFolder = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            string fileName = $"{binaryFolder}/youtube-dl.exe";
            string arguments = $"-x --audio-format mp3 --audio-quality 0 --embed-thumbnail --no-part -c -o \"{audio.DestinationPath}/{audio.Title}.%(ext)s\" \"{YouTubeUrl}{audio.Id}\"";
            _logger.Debug($"Starting youtube-dl process with parameters: {fileName} {arguments}");

            Process process = new Process
            {
                StartInfo =
                {
                    FileName = fileName,
                    Arguments = arguments,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                }
            };

            process.OutputDataReceived += OutputHandler;
            process.ErrorDataReceived += ErrorHandler;

            return process;
        }

        private void ErrorHandler(object sendingProcess, DataReceivedEventArgs error)
        {
            if (error.Data != null)
            {
                _logger.Warn($"youtube-dl.exe: {error.Data}");
            }
        }

        public void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (outLine.Data != null)
            {
                _logger.Debug($"youtube-dl.exe: {outLine.Data}");
            }
        }
    }
}
