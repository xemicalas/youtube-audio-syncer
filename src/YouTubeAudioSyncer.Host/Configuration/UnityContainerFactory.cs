﻿using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Host.Configuration.Modules;

namespace YouTubeAudioSyncer.Host.Configuration
{
    public class UnityContainerFactory
    {
        public static IUnityContainer Create()
        {
            UnityContainer container = new UnityContainer();
            container.AddExtension(new ClientModule());
            container.AddExtension(new SchedulerModule());
            container.AddExtension(new StorageModule());
            container.AddExtension(new DownloaderModule());
            container.AddExtension(new ExtensionsModule());
            container.AddExtension(new SubscriptionsModule());
            container.AddExtension(new SyncerModule());
            container.AddExtension(new WebApiServiceModule());
            container.AddExtension(new ControllersModule());
            return container;
        }
    }
}