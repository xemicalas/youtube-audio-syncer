﻿using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.WebApi.Controllers;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class ControllersModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<AudioController>(new PerResolveLifetimeManager());
            Container.RegisterType<AudioSynchronizationController>(new PerResolveLifetimeManager());
        }
    }
}