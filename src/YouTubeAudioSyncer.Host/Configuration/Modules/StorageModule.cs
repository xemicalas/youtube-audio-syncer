﻿using System.Configuration;
using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Interfaces;
using YouTubeAudioSyncer.Storage;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class StorageModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            string registryStoragePath = ConfigurationManager.AppSettings["registry.storage.path"];

            Container.RegisterType<IValueStorage, RegistryValueStorage>(new ContainerControlledLifetimeManager(),
                new InjectionFactory(c => new RegistryValueStorage(registryStoragePath)));

            Container.RegisterType<ILastSyncDateProvider, LastSyncDateProvider>(new ContainerControlledLifetimeManager());
        }
    }
}