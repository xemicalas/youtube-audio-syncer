﻿using System;
using System.Configuration;
using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Extensions.Winamp;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class ExtensionsModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            bool enableWinampExtension = Convert.ToBoolean(ConfigurationManager.AppSettings["enable.winamp.extension"]);
            if (enableWinampExtension)
            {
                string winampPlayListDirectory = ConfigurationManager.AppSettings["winamp.playlist.directory.path"];
                Container.RegisterType<IWinampPlayListSyncer, WinampPlayListSyncer>(new ContainerControlledLifetimeManager(),
                    new InjectionFactory(c => new WinampPlayListSyncer(winampPlayListDirectory)));
            }
        }
    }
}