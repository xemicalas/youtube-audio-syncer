﻿using System.Configuration;
using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Client;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class ClientModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            string youTubeApiKey = ConfigurationManager.AppSettings["youtube.api.key"];

            Container.RegisterType<IYouTubeClient, YouTubeClient>(new ContainerControlledLifetimeManager(),
                new InjectionFactory(c => new YouTubeClient(youTubeApiKey)));
        }
    }
}