﻿using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Downloader;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class DownloaderModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<IAudioDownloader, YouTubeAudioDownloader>(new PerResolveLifetimeManager());
        }
    }
}