﻿using System;
using System.Configuration;
using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Interfaces;
using YouTubeAudioSyncer.Subscriptions;
using YouTubeAudioSyncer.Subscriptions.Subscriptions;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class SubscriptionsModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            string targetDirectoryPath = ConfigurationManager.AppSettings["target.directory.path"];
            bool enableWinampExtension = Convert.ToBoolean(ConfigurationManager.AppSettings["enable.winamp.extension"]);

            Container.RegisterType<IAudioSubscriptionsNotifier, AudioSubscriptionsNotifier>(new ContainerControlledLifetimeManager());

            Container.RegisterType<IAudioSubscription, AudioTitleNormalizer>("AudioTitleNormalizerSubscription", new ContainerControlledLifetimeManager());
            Container.RegisterType<IAudioSubscription, AdditionalAudioDataPopulator>("AdditionalAudioDataPopulator", new ContainerControlledLifetimeManager(),
                new InjectionFactory(c => new AdditionalAudioDataPopulator(targetDirectoryPath)));
            Container.RegisterType<IAudioSubscription, AudioDownloader>("AudioDownloaderSubscription", new ContainerControlledLifetimeManager());
            Container.RegisterType<IAudioSubscription, AudioMetadataPopulator>("AudioMetadataPopulatorSubscription", new ContainerControlledLifetimeManager());
            if (enableWinampExtension)
            {
                Container.RegisterType<IAudioSubscription, WinampPlayListSyncer>("WinampPlayListSyncerSubscription", new ContainerControlledLifetimeManager());
            }
            Container.RegisterType<IAudioSubscription, AudioFileMigrator>("AudioFileMigrator", new ContainerControlledLifetimeManager());
        }
    }
}