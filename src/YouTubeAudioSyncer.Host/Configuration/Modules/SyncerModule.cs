﻿using System.Configuration;
using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Interfaces;
using YouTubeAudioSyncer.Syncer;
using YouTubeAudioSyncer.Syncer.Configuration;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class SyncerModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            string playListId = ConfigurationManager.AppSettings["youtube.playlist.id"];

            Container.RegisterInstance(new SyncerOptions
            {
                PlayListId = playListId
            });

            Container.RegisterType<ISyncer, AudioSyncer>(new ContainerControlledLifetimeManager());
        }
    }
}