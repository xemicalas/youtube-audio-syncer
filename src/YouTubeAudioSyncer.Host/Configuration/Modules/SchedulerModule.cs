﻿using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Interfaces;
using YouTubeAudioSyncer.Scheduler;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class SchedulerModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<IJobScheduler, AudioSyncJobScheduler>(new ContainerControlledLifetimeManager());
        }
    }
}