﻿using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Interfaces;
using YouTubeAudioSyncer.WebApi;
using YouTubeAudioSyncer.WebApi.Configuration;

namespace YouTubeAudioSyncer.Host.Configuration.Modules
{
    public class WebApiServiceModule : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<IDependencyResolver, UnityDependencyResolver>();
            Container.RegisterType<IHttpConfigurationFactory, WebApiConfigFactory>(new ContainerControlledLifetimeManager(),
                new InjectionFactory(c => new WebApiConfigFactory(
                    new DefaultRoutesRegistrator(),
                    new DependencyResolverRegistrator(c.Resolve<IDependencyResolver>()),
                    new SwaggerRegistrator())));
            Container.RegisterType<IWebApiConfigOptions, WebApiConfigOptions>();
        }
    }
}