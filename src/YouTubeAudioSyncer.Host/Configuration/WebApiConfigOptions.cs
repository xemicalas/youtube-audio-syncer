﻿using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Host.Configuration
{
    public class WebApiConfigOptions : IWebApiConfigOptions
    {
        public string Name { get; set; } = "YouTube Audio Syncer";
        public int Port { get; set; } = 21223;
    }
}
