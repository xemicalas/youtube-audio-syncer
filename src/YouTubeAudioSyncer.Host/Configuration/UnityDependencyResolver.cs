﻿using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;

namespace YouTubeAudioSyncer.Host.Configuration
{
    public class UnityDependencyResolver : UnityScopeContainer, IDependencyResolver
    {
        public UnityDependencyResolver(IUnityContainer container)
            : base(container)
        {
        }

        public IDependencyScope BeginScope()
        {
            var childContainer = Container.CreateChildContainer();
            return new UnityScopeContainer(childContainer);
        }
    }
}
