﻿using System;
using System.Collections.Generic;
using Common.Logging;
using Microsoft.Practices.Unity;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Host
{
    public class MultipleServiceHost
    {
        private readonly IUnityContainer _container;
        private readonly List<IService> _services = new List<IService>();
        private readonly ILog _logger;

        public MultipleServiceHost(IUnityContainer container, params Type[] services)
        {
            _container = container;
            _logger = LogManager.GetLogger(GetType());
            foreach (Type serviceType in services)
            {
                IService service = (IService)_container.Resolve(serviceType);
                _services.Add(service);
            }
        }

        public void Start()
        {
            _logger.Info("Starting services...");
            foreach (IService service in _services)
            {
                service.Start();
                _logger.Info($"{service.GetType()} started.");
            }
        }

        public void Stop()
        {
            _logger.Info("Stopping services...");
            _container.Dispose();
            foreach (IService service in _services)
            {
                service.Stop();
                _logger.Info($"{service.GetType()} stopped.");
            }
        }
    }
}