﻿using Microsoft.Practices.Unity;
using Topshelf;
using Topshelf.ServiceConfigurators;
using YouTubeAudioSyncer.Host.Configuration;
using YouTubeAudioSyncer.Scheduler;
using YouTubeAudioSyncer.WebApi;

namespace YouTubeAudioSyncer.Host
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            HostFactory.Run(hostConfigurator =>
            {
                hostConfigurator.Service<MultipleServiceHost>(RunServices);

                hostConfigurator.RunAsLocalSystem();
                hostConfigurator.StartAutomatically();

                hostConfigurator.SetServiceName("YoutubeAudioSyncer");
                hostConfigurator.SetDescription("Youtube Audio syncer service.");
                hostConfigurator.SetDisplayName("Youtube Audio Syncer");
            });
        }

        private static void RunServices(ServiceConfigurator<MultipleServiceHost> configurator)
        {
            configurator.ConstructUsing(() =>
            {
                IUnityContainer container = UnityContainerFactory.Create();
                return new MultipleServiceHost(container,
                    typeof(SchedulerService),
                    typeof(WebApiService));
            });

            configurator.WhenStarted(service => service.Start());
            configurator.WhenStopped(service => service.Stop());
        }
    }
}