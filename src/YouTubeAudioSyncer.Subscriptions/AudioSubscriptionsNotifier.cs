﻿using System.Threading.Tasks;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Subscriptions
{
    public class AudioSubscriptionsNotifier : IAudioSubscriptionsNotifier
    {
        private readonly IAudioSubscription[] _subscriptions;

        public AudioSubscriptionsNotifier(IAudioSubscription[] subscriptions)
        {
            _subscriptions = subscriptions;
        }

        public async Task Notify(Audio audio)
        {
            foreach (IAudioSubscription subscription in _subscriptions)
            {
                await subscription.Notify(audio).ConfigureAwait(false);
            }
        }
    }
}