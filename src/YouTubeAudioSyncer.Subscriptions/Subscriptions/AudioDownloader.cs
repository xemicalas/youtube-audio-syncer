﻿using System.IO;
using System.Threading.Tasks;
using Common.Logging;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Subscriptions.Subscriptions
{
    public class AudioDownloader : IAudioSubscription
    {
        private readonly IAudioDownloader _downloader;
        private readonly ILog _logger;

        public AudioDownloader(IAudioDownloader downloader)
        {
            _downloader = downloader;
            _logger = LogManager.GetLogger(GetType());
        }

        public Task Notify(Audio audioClip)
        {
            DeleteAudioTempDirectory(audioClip);
            DownloadableAudio downloadableAudio = MapAudio(audioClip);
            return _downloader.Download(downloadableAudio);
        }

        private DownloadableAudio MapAudio(Audio audio)
        {
            return new DownloadableAudio
            {
                Id = audio.Id,
                Title = audio.Title,
                DestinationPath = audio.TempLocation.DirectoryPath
            };
        }

        private void DeleteAudioTempDirectory(Audio audio)
        {
            string audioTempFolder = audio.TempLocation.DirectoryPath;
            if (Directory.Exists(audioTempFolder))
            {
                _logger.Debug($"Temp folder \"{audioTempFolder}\" exists, deleting folder before downloading the audio.");
                Directory.Delete(audioTempFolder, true);
            }
        }
    }
}