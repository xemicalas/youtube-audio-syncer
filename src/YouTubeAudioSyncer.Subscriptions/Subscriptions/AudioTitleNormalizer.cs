﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Common.Logging;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Subscriptions.Subscriptions
{
    public class AudioTitleNormalizer : IAudioSubscription
    {
        private const string MatchWordsToRemove = @"(\[[\w\s]+\]$|\(original mix\)|\(.*?video.*?\)|\/? (official )?((music|lyric) )?video( edit)?)";
        private const string MatchForbiddenCharacters = @"[\\/\:\*\?\<\>\|]";

        private readonly ILog _logger;

        public AudioTitleNormalizer()
        {
            _logger = LogManager.GetLogger(GetType());
        }

        public Task Notify(Audio audioClip)
        {
            NormalizeTitle(audioClip);
            return Task.CompletedTask;
        }

        private void NormalizeTitle(Audio audioClip)
        {
            _logger.Info("Normalizing audio title...");

            audioClip.Title = RemoveUnwantedWords(audioClip.Title);
            audioClip.Title = RemoveForbiddenCharacters(audioClip.Title);

            Regex whiteSpacesMatch = new Regex(@"\s{2,}", RegexOptions.None);
            audioClip.Title = whiteSpacesMatch.Replace(audioClip.Title, " ");
            audioClip.Title = audioClip.Title.Replace('–', '-').Trim();
        }

        private static string RemoveUnwantedWords(string title)
        {
            Regex partToMatch = new Regex(MatchWordsToRemove, RegexOptions.IgnoreCase);
            return partToMatch.Replace(title, string.Empty);
        }

        private static string RemoveForbiddenCharacters(string title)
        {
            Regex partToMatch = new Regex(MatchForbiddenCharacters, RegexOptions.IgnoreCase);
            return partToMatch.Replace(title, string.Empty);
        }
    }
}