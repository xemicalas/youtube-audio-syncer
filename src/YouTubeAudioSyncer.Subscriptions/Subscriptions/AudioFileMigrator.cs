﻿using System.IO;
using System.Threading.Tasks;
using Common.Logging;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Subscriptions.Subscriptions
{
    public class AudioFileMigrator : IAudioSubscription
    {
        private readonly ILog _logger;

        public AudioFileMigrator()
        {
            _logger = LogManager.GetLogger(GetType());
        }

        public Task Notify(Audio audioClip)
        {
            MoveAudioFile(audioClip);
            DeleteAudioTempDirectory(audioClip);
            return Task.CompletedTask;
        }

        private void MoveAudioFile(Audio audio)
        {
            string sourcePath = audio.TempLocation.FullPath;
            string destinationPath = audio.OriginLocation.FullPath;

            _logger.Debug($"Moving audio from \"{sourcePath}\" to \"{destinationPath}\".");
            if (File.Exists(destinationPath))
            {
                _logger.Debug($"Audio in \"{destinationPath}\" already exists, deleting...");
                File.Delete(destinationPath);
            }
            if (!Directory.Exists(audio.OriginLocation.DirectoryPath))
            {
                _logger.Debug($"Directory {audio.OriginLocation.DirectoryPath} does not exist. Creating..");
                Directory.CreateDirectory(audio.OriginLocation.DirectoryPath);
            }
            File.Move(sourcePath, destinationPath);
        }

        private void DeleteAudioTempDirectory(Audio audio)
        {
            string audioTempFolder = audio.TempLocation.DirectoryPath;

            _logger.Debug($"Deleting audio temp directory \"{audioTempFolder}\".");
            Directory.Delete(audioTempFolder, true);
        }
    }
}