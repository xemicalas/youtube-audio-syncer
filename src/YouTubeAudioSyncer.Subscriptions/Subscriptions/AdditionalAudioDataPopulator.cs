﻿using System;
using System.IO;
using System.Threading.Tasks;
using Common.Logging;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Subscriptions.Subscriptions
{
    public class AdditionalAudioDataPopulator : IAudioSubscription
    {
        private readonly string _targetPath;
        private readonly ILog _logger;

        public AdditionalAudioDataPopulator(string targetPath)
        {
            _targetPath = targetPath;
            _logger = LogManager.GetLogger(GetType());
        }

        public Task Notify(Audio audioClip)
        {
            PopulateAudioData(audioClip);
            return Task.CompletedTask;
        }

        private void PopulateAudioData(Audio audio)
        {
            _logger.Info($"Populating audio \"{audio.Title}\" with additional data.");
            audio.Extension = "mp3";

            DateTime currentDate = DateTime.Now;
            string originDirectoryPath = $"{_targetPath}/{currentDate:yyyy}/{currentDate:yyyy-MM}";
            audio.OriginLocation = new MediaLocation
            {
                DirectoryPath = originDirectoryPath,
                FullPath = $"{originDirectoryPath}/{audio.Title}.{audio.Extension}"
            };

            string tempDirectoryPath = GetAudioTempDirectoryPath(audio);
            audio.TempLocation = new MediaLocation
            {
                DirectoryPath = tempDirectoryPath,
                FullPath = $"{tempDirectoryPath}/{audio.Title}.{audio.Extension}"
            };
        }

        private string GetAudioTempDirectoryPath(Audio audio)
        {
            return $"{Path.GetTempPath()}YouTubeAudioSyncer/{audio.Id}";
        }
    }
}
