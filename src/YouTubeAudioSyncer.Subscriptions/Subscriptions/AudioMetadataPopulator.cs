﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Logging;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;
using File = TagLib.File;

namespace YouTubeAudioSyncer.Subscriptions.Subscriptions
{
    public class AudioMetadataPopulator : IAudioSubscription
    {
        private readonly ILog _logger;

        public AudioMetadataPopulator()
        {
            TagLib.Id3v2.Tag.DefaultVersion = 3;
            TagLib.Id3v2.Tag.ForceDefaultVersion = true;
            _logger = LogManager.GetLogger(GetType());
        }

        public Task Notify(Audio audioClip)
        {
            UpdateAudioMetadata(audioClip);
            return Task.CompletedTask;
        }

        private void UpdateAudioMetadata(Audio audioClip)
        {
            _logger.Info($"Populating audio \"{audioClip.Title}\" metadata.");
            try
            {
                File file = File.Create(audioClip.TempLocation.FullPath);
                file.Tag.Performers = GetAlbumArtists(audioClip).ToArray();
                file.Tag.Title = GetAudioTitle(audioClip);
                file.Save();
            }
            catch (Exception ex)
            {
                throw new AudioSynchronizationException("Failed to save audio metadata.", ex);
            }
        }

        private List<string> GetAlbumArtists(Audio audioClip)
        {
            List<string> albumArtists = new List<string>
            {
                audioClip.Title
            };
            string[] parts = audioClip.Title.Split(new[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length >= 1)
            {
                string albumArtistSegment = parts[0].Trim();
                albumArtists = ExtractAlbumArtists(albumArtistSegment);
            }
            return albumArtists;
        }

        private List<string> ExtractAlbumArtists(string albumArtistSegment)
        {
            string[] artists = albumArtistSegment.Split(new[] { ",", "&", "ft.", "ft", "Ft.", "Ft", "feat.", "feat", "Feat.", "Feat" }, StringSplitOptions.RemoveEmptyEntries);
            return artists.Select(artist => artist.Trim()).ToList();
        }

        private string GetAudioTitle(Audio audioClip)
        {
            string audioTitle = audioClip.Title;
            string[] parts = audioClip.Title.Split(new[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length < 2)
            {
                parts = audioClip.Title.Split('-');
            }
            if (parts.Length >= 2)
            {
                audioTitle = parts[1].Trim();
            }
            return audioTitle;
        }
    }
}