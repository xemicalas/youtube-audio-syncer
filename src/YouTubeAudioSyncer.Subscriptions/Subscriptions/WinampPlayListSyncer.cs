﻿using System.Threading.Tasks;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Extensions.Winamp;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Subscriptions.Subscriptions
{
    public class WinampPlayListSyncer : IAudioSubscription
    {
        private readonly IWinampPlayListSyncer _winampSyncer;

        public WinampPlayListSyncer(IWinampPlayListSyncer winampSyncer)
        {
            _winampSyncer = winampSyncer;
        }

        public Task Notify(Audio audioClip)
        {
            _winampSyncer.Synchronize(audioClip);
            return Task.CompletedTask;
        }
    }
}