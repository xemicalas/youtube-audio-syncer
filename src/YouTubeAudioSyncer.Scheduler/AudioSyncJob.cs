﻿using System;
using Common.Logging;
using Quartz;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Scheduler
{
    [DisallowConcurrentExecution]
    public class AudioSyncJob : IJob
    {
        private static readonly TimeSpan JobValidityDuration = TimeSpan.FromSeconds(5);
        private readonly ILog _logger;

        public AudioSyncJob()
        {
            _logger = LogManager.GetLogger(GetType());
        }

        public void Execute(IJobExecutionContext context)
        {
            if (!IsJobExpired(context))
            {
                ISyncer syncer = (ISyncer) context.Scheduler.Context.Get("AudioSyncer");
                syncer.Synchronize().Wait();
            }
            else
            {
                _logger.Debug("Received expired job. Skipping job execution.");
            }
        }

        private bool IsJobExpired(IJobExecutionContext context)
        {
            if (context.ScheduledFireTimeUtc != null && context.FireTimeUtc != null)
            {
                DateTimeOffset expirationDate = context.FireTimeUtc.Value.Subtract(JobValidityDuration);
                if (context.ScheduledFireTimeUtc.Value > expirationDate)
                {
                    return false;
                }
            }
            return true;
        }
    }
}