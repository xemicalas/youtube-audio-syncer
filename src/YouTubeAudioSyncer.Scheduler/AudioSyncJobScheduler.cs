﻿using Quartz;
using Quartz.Impl;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Scheduler
{
    public class AudioSyncJobScheduler : IJobScheduler
    {
        private readonly IScheduler _scheduler;

        public AudioSyncJobScheduler(ISyncer syncer)
        {
            _scheduler = StdSchedulerFactory.GetDefaultScheduler();
            _scheduler.Context.Put("AudioSyncer", syncer);
        }

        public void Start()
        {
            IJobDetail job = JobBuilder.Create<AudioSyncJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule(x =>
                {
                    x.WithIntervalInSeconds(15)
                        .WithMisfireHandlingInstructionNextWithRemainingCount()
                        .RepeatForever();
                })
                .Build();

            _scheduler.Start();
            _scheduler.ScheduleJob(job, trigger);
        }

        public void Shutdown()
        {
            _scheduler.Shutdown();
        }
    }
}