﻿using Common.Logging;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.Scheduler
{
    public class SchedulerService : IService
    {
        private readonly IJobScheduler _jobScheduler;
        private readonly ILog _logger;

        public SchedulerService(IJobScheduler jobScheduler)
        {
            _jobScheduler = jobScheduler;
            _logger = LogManager.GetLogger(GetType());
        }

        public void Start()
        {
            _logger.Info("Starting synchronization job scheduler.");
            _jobScheduler.Start();
        }

        public void Stop()
        {
            _logger.Info("Stopping synchronization job scheduler.");
            _jobScheduler.Shutdown();
        }
    }
}