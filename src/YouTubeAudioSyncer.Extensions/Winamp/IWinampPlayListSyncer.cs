﻿using YouTubeAudioSyncer.Contracts;

namespace YouTubeAudioSyncer.Extensions.Winamp
{
    public interface IWinampPlayListSyncer
    {
        void Synchronize(Audio audioClip);
    }
}