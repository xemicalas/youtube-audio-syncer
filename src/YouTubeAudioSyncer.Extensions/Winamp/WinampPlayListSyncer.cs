﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Common.Logging;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using YouTubeAudioSyncer.Contracts;

namespace YouTubeAudioSyncer.Extensions.Winamp
{
    public class WinampPlayListSyncer : IWinampPlayListSyncer
    {
        private readonly string _winampPlayListDirectory;
        private readonly string _playListRootFilePath;
        private readonly ILog _logger;

        private readonly object _syncLock = new object();

        public WinampPlayListSyncer(string winampPlayListDirectory)
        {
            _winampPlayListDirectory = winampPlayListDirectory;
            _playListRootFilePath = $"{_winampPlayListDirectory}/playlists.xml";
            _logger = LogManager.GetLogger(GetType());
        }

        public void Synchronize(Audio audioClip)
        {
            lock (_syncLock)
            {
                SyncWithPlayList(audioClip);
            }
        }

        private void SyncWithPlayList(Audio audioClip)
        {
            _logger.Info($"Synchronizing audio \"{audioClip.Title}\" with Winamp library.");
            string playListFileName = GetDestinationPlayListFileName(audioClip);
            if (!AudioExistsInPlayList(playListFileName, audioClip))
            {
                _logger.Debug($"Audio \"{audioClip.Title}\" does not exists, adding to \"{playListFileName}\" library.");
                AddAudioToPlayList(playListFileName, audioClip);
                UpdateRootFile(audioClip);
            }
        }

        private string GetDestinationPlayListFileName(Audio audioClip)
        {
            string playListFileName;
            string playListDirectoryName = GetPlayListDirectoryName(audioClip);
            XDocument document = XDocument.Load(_playListRootFilePath);
            XElement element = document.Root?.Elements().FirstOrDefault(x => x.Attribute("title")?.Value.Equals(playListDirectoryName) ?? false);
            if (element == null)
            {
                playListFileName = $"{playListDirectoryName}.m3u8";
                _logger.Debug($"Audio library \"{playListFileName}\" does not exists. Creating...");
                CreatePlayListFile(playListFileName);
                AddPlayListToRootFile(playListFileName, playListDirectoryName);
            }
            else
            {
                playListFileName = element.Attribute("filename")?.Value;
            }

            return playListFileName;
        }

        private bool AudioExistsInPlayList(string playListFileName, Audio audioClip)
        {
            bool exists = false;
            using (StreamReader sr = new StreamReader($"{_winampPlayListDirectory}/{playListFileName}"))
            {
                while (sr.Peek() >= 0)
                {
                    string line = sr.ReadLine() ?? string.Empty;
                    if (line.EndsWith(audioClip.Title))
                    {
                        exists = true;
                        break;
                    }
                }
            }
            return exists;
        }

        private void AddAudioToPlayList(string playListFileName, Audio audioClip)
        {
            using (StreamWriter sw = new StreamWriter($"{_winampPlayListDirectory}/{playListFileName}", true))
            {
                int audioDurationInSeconds = GetAudioDurationInSeconds(audioClip);
                sw.WriteLine($"#EXTINF:{audioDurationInSeconds},{audioClip.Title}");
                sw.WriteLine(audioClip.OriginLocation.FullPath.Replace("/", "\\"));
            }
        }

        private void UpdateRootFile(Audio audioClip)
        {
            string playListDirectoryName = GetPlayListDirectoryName(audioClip);
            XDocument document = XDocument.Load(_playListRootFilePath);
            XElement element = document.Root?.Elements().FirstOrDefault(x => x.Attribute("title")?.Value.Equals(playListDirectoryName) ?? false);
            if (element != null)
            {
                int songCount = int.Parse(element.Attribute("songs")?.Value ?? "0");
                element.SetAttributeValue("songs", ++songCount);

                int totalDuration = int.Parse(element.Attribute("seconds")?.Value ?? "0");
                int audioDurationInSeconds = GetAudioDurationInSeconds(audioClip);
                element.SetAttributeValue("seconds", totalDuration + audioDurationInSeconds);

                document.Save(_playListRootFilePath);
            }
        }

        private void AddPlayListToRootFile(string playListFileName, string playListTitle)
        {
            XDocument document = XDocument.Load(_playListRootFilePath);
            document.Root?.Add(
                new XElement("playlist",
                    new XAttribute("filename", playListFileName),
                    new XAttribute("title", playListTitle),
                    new XAttribute("id", string.Concat("{", Guid.NewGuid().ToString().ToUpper(), "}")),
                    new XAttribute("songs", 0),
                    new XAttribute("seconds", 0)
                ));

            int totalPlayList = int.Parse(document.Root?.Attribute("playlists")?.Value ?? "0");
            document.Root?.SetAttributeValue("playlists", ++totalPlayList);

            document.Save(_playListRootFilePath);
        }

        private void CreatePlayListFile(string playListFileName)
        {
            using (StreamWriter sw = new StreamWriter($"{_winampPlayListDirectory}/{playListFileName}"))
            {
                sw.WriteLine("#EXTM3U");
            }
        }

        private int GetAudioDurationInSeconds(Audio audioClip)
        {
            using (ShellObject shell = ShellObject.FromParsingName(audioClip.TempLocation.FullPath.Replace('/', '\\')))
            {
                IShellProperty prop = shell.Properties.System.Media.Duration;
                ulong value = (ulong)prop.ValueAsObject;
                return (int)TimeSpan.FromTicks((long)value).TotalSeconds;
            }
        }

        private string GetPlayListDirectoryName(Audio audio)
        {
            string originPath = audio.OriginLocation.DirectoryPath;
            return originPath.Split('/').LastOrDefault();
        }
    }
}