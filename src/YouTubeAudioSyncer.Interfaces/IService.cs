﻿namespace YouTubeAudioSyncer.Interfaces
{
    public interface IService
    {
        void Start();
        void Stop();
    }
}