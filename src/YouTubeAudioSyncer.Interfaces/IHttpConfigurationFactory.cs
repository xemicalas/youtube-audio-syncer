﻿using System.Web.Http;

namespace YouTubeAudioSyncer.Interfaces
{
    public interface IHttpConfigurationFactory
    {
        HttpConfiguration Create(IWebApiConfigOptions options);
    }
}