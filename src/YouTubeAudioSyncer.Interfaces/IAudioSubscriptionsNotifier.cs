﻿using System.Threading.Tasks;
using YouTubeAudioSyncer.Contracts;

namespace YouTubeAudioSyncer.Interfaces
{
    public interface IAudioSubscriptionsNotifier
    {
        Task Notify(Audio audio);
    }
}