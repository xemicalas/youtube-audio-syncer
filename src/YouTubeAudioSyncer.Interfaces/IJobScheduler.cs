﻿namespace YouTubeAudioSyncer.Interfaces
{
    public interface IJobScheduler
    {
        void Start();
        void Shutdown();
    }
}