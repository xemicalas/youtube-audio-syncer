﻿namespace YouTubeAudioSyncer.Interfaces
{
    public interface IValueStorage
    {
        object GetValue(string key);
        void PutValue(string key, object value);
    }
}
