﻿using System.Collections.Generic;
using System.Threading.Tasks;
using YouTubeAudioSyncer.Contracts;

namespace YouTubeAudioSyncer.Interfaces
{
    public interface IYouTubeClient
    {
        Task<Audio> GetAudioClip(string audioId);
        Task<List<Audio>> GetAudioClips(string playListId, int limit);
    }
}