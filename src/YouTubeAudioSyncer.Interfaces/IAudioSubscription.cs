﻿using System.Threading.Tasks;
using YouTubeAudioSyncer.Contracts;

namespace YouTubeAudioSyncer.Interfaces
{
    public interface IAudioSubscription
    {
        Task Notify(Audio audioClip);
    }
}