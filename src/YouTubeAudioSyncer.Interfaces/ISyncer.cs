﻿using System.Threading.Tasks;

namespace YouTubeAudioSyncer.Interfaces
{
    public interface ISyncer
    {
        Task Synchronize();
    }
}