﻿using System.Threading.Tasks;
using YouTubeAudioSyncer.Contracts;

namespace YouTubeAudioSyncer.Interfaces
{
    public interface IAudioDownloader
    {
        Task Download(DownloadableAudio audio);
    }
}