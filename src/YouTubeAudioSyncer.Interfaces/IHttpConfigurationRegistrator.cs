﻿using System.Web.Http;

namespace YouTubeAudioSyncer.Interfaces
{
    public interface IHttpConfigurationRegistrator
    {
        void Use(HttpConfiguration config, IWebApiConfigOptions options);
    }
}