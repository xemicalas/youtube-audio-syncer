﻿namespace YouTubeAudioSyncer.Interfaces
{
    public interface IWebApiConfigOptions
    {
        string Name { get; set; }
        int Port { get; set; }
    }
}