﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Logging;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;
using YouTubeAudioSyncer.Storage;
using YouTubeAudioSyncer.Syncer.Configuration;

namespace YouTubeAudioSyncer.Syncer
{
    public class AudioSyncer : ISyncer
    {
        private const int MaxItemsToRetrieve = 50;

        private readonly SyncerOptions _options;
        private readonly IYouTubeClient _youTubeClient;
        private readonly ILastSyncDateProvider _dateProvider;
        private readonly IAudioSubscriptionsNotifier _notifier;
        private readonly ILog _logger;

        public AudioSyncer(IYouTubeClient youTubeClient,
            ILastSyncDateProvider dateProvider,
            IAudioSubscriptionsNotifier notifier,
            SyncerOptions options)
        {
            _options = options;
            _youTubeClient = youTubeClient;
            _dateProvider = dateProvider;
            _notifier = notifier;
            _logger = LogManager.GetLogger(GetType());
        }

        public async Task Synchronize()
        {
            List<Audio> audioClips = await _youTubeClient.GetAudioClips(_options.PlayListId, MaxItemsToRetrieve).ConfigureAwait(false);
            DateTime lastSyncDate = _dateProvider.Get();
            List<Audio> desynchronizedAudioClips = audioClips.Where(item => item.PublishedAt > lastSyncDate).OrderBy(x => x.PublishedAt).ToList();
            if (desynchronizedAudioClips.Count > 0)
            {
                _logger.Debug($"Found {desynchronizedAudioClips.Count} desynchronized audio clips. Synchronizing...");
                bool shouldRetry = true;
                while (shouldRetry)
                {
                    shouldRetry = false;
                    try
                    {
                        await NotifySubscribers(desynchronizedAudioClips).ConfigureAwait(false);
                    }
                    catch (AudioSynchronizationException ex)
                    {
                        _logger.Warn($"Retrying failed audio clips synchronization. Reason: {ex.Message} {ex.InnerException}");
                        shouldRetry = true;
                    }
                }
                _logger.Info("Synchronization completed.");
            }
        }

        private Task NotifySubscribers(List<Audio> audioClips)
        {
            List<Task> notifyTasks = new List<Task>();
            foreach (Audio audioClip in audioClips)
            {
                notifyTasks.Add(_notifier.Notify(audioClip));
            }
            return Task.WhenAll(notifyTasks);
        }
    }
}