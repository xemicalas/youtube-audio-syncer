﻿using System.Web.Http;
using System.Web.Http.Dependencies;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.WebApi.Configuration
{
    public class DependencyResolverRegistrator : IHttpConfigurationRegistrator
    {
        private readonly IDependencyResolver _resolver;

        public DependencyResolverRegistrator(IDependencyResolver resolver)
        {
            _resolver = resolver;
        }

        public void Use(HttpConfiguration config, IWebApiConfigOptions options)
        {
            config.DependencyResolver = _resolver;
        }
    }
}