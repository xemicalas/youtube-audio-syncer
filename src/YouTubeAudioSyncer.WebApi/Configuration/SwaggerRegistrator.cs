﻿using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using Swashbuckle.Application;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.WebApi.Configuration
{
    public class SwaggerRegistrator : IHttpConfigurationRegistrator
    {
        public void Use(HttpConfiguration config, IWebApiConfigOptions options)
        {
            config
                .EnableSwagger(
                    c =>
                    {
                        c.SingleApiVersion("v1", options.Name);
                        c.UseFullTypeNameInSchemaIds();

                        c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                        IncludeXmlComments(c);
                    })
                .EnableSwaggerUi(
                    c =>
                    {
                        c.DisableValidator();
                    });
        }

        private void IncludeXmlComments(SwaggerDocsConfig swgConfig)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string commentsFileName = "YouTubeAudioSyncer.WebApi.xml";
            string commentsFile = Path.Combine(baseDirectory, commentsFileName);

            string contractCommentsFileName = "YouTubeAudioSyncer.WebApi.Contracts.xml";
            string contractCommentsFile = Path.Combine(baseDirectory, contractCommentsFileName);

            swgConfig.IncludeXmlComments(commentsFile);
            swgConfig.IncludeXmlComments(contractCommentsFile);
        }
    }
}