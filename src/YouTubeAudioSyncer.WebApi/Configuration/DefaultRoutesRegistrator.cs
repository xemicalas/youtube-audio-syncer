﻿using System.Web.Http;
using System.Web.Http.Cors;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.WebApi.Configuration
{
    public class DefaultRoutesRegistrator : IHttpConfigurationRegistrator
    {
        public void Use(HttpConfiguration config, IWebApiConfigOptions options)
        {
            var cors = new EnableCorsAttribute("*", "*", "*") { SupportsCredentials = true };
            config.EnableCors(cors);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "v1/{controller}/{id}",
                new {id = RouteParameter.Optional}
            );
        }
    }
}