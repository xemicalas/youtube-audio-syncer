﻿using System;
using System.Web.Http;
using Microsoft.Owin.Hosting;
using Owin;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.WebApi
{
    public class WebApiService : IService
    {
        private readonly IWebApiConfigOptions _options;
        private readonly IHttpConfigurationFactory _configurationFactory;
        private IDisposable _webApiService;

        public WebApiService(IWebApiConfigOptions options, IHttpConfigurationFactory configurationFactory)
        {
            _options = options;
            _configurationFactory = configurationFactory;
        }

        public void Start()
        {
            StartOptions startOptions = new StartOptions($"http://*:{_options.Port}/");
            _webApiService = WebApp.Start(startOptions, BuildApp);
        }

        private void BuildApp(IAppBuilder builder)
        {
            HttpConfiguration webApiConfig = _configurationFactory.Create(_options);
            builder.UseWebApi(webApiConfig);
        }

        public void Stop()
        {
            _webApiService?.Dispose();
        }
    }
}
