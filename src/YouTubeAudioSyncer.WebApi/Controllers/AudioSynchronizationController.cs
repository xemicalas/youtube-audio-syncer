﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.WebApi.Controllers
{
    /// <summary>
    /// Audio Synchronization Controller
    /// </summary>
    public class AudioSynchronizationController : ApiController
    {
        private readonly ISyncer _syncer;

        public AudioSynchronizationController(ISyncer syncer)
        {
            _syncer = syncer;
        }

        /// <summary>
        /// Synchronizes audio from YouTube to local PC
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("v1/synchronize")]
        [SwaggerResponseRemoveDefaults]
        [SwaggerResponse(HttpStatusCode.Accepted)]
        public IHttpActionResult SynchronizeAudioClips()
        {
            Task.Run(() => _syncer.Synchronize());
            return StatusCode(HttpStatusCode.Accepted);
        }
    }
}
