﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using YouTubeAudioSyncer.Contracts;
using YouTubeAudioSyncer.Interfaces;
using YouTubeAudioSyncer.WebApi.Contracts;

namespace YouTubeAudioSyncer.WebApi.Controllers
{
    /// <summary>
    /// Audio controller
    /// </summary>
    public class AudioController : ApiController
    {
        private readonly IYouTubeClient _youTubeClient;
        private readonly IAudioSubscriptionsNotifier _notifier;

        public AudioController(IYouTubeClient youTubeClient, IAudioSubscriptionsNotifier notifier)
        {
            _youTubeClient = youTubeClient;
            _notifier = notifier;
        }

        /// <summary>
        /// Download audio to the newest playlist folder
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("v1/audio/download")]
        [SwaggerResponseRemoveDefaults]
        [SwaggerResponse(HttpStatusCode.Accepted)]
        public async Task<IHttpActionResult> DownloadAudio(AudioDownloadData audioDownloadData)
        {
            Audio audio = await _youTubeClient.GetAudioClip(audioDownloadData.AudioId).ConfigureAwait(false);
            if (audio == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            NotifyAudioDownloadAsync(audio);

            return StatusCode(HttpStatusCode.Accepted);
        }

        private void NotifyAudioDownloadAsync(Audio audio)
        {
            _notifier.Notify(audio);
        }
    }
}