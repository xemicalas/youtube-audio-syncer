﻿using System.Web.Http;
using YouTubeAudioSyncer.Interfaces;

namespace YouTubeAudioSyncer.WebApi
{
    public class WebApiConfigFactory : IHttpConfigurationFactory
    {
        private readonly IHttpConfigurationRegistrator[] _registrators;

        public WebApiConfigFactory(params IHttpConfigurationRegistrator[] registrators)
        {
            _registrators = registrators;
        }

        public HttpConfiguration Create(IWebApiConfigOptions options)
        {
            HttpConfiguration config = new HttpConfiguration();
            foreach (IHttpConfigurationRegistrator registrator in _registrators)
            {
                registrator.Use(config, options);
            }
            return config;
        }
    }
}
